import React, { Component } from 'react';
export default class Comment extends Component {
  render() {
    return (
        <div className='row py-0'>
          <div className='col-md-1'>&nbsp;</div>
          <div className='col-md-11'>
            <div className='card border-secondary mb-1'>
              <div className='card-body py-0'>
                <p className='card-text'>
                  <strong>{this.props.userName}</strong>: &nbsp;
                  <small>{this.props.body}</small>
                </p>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
