import React, { Component } from 'react';

export default class Album extends Component {
  render() {
    return (
        <React.Fragment>
          <div className='row border-top border-left'>
            <div className='col-md-12'>
              <p>{this.props.title}</p>
            </div>
            <div className='col-md-12'>
              <p>
                <img src={this.props.imageSrc} className='rounded mx-auto d-block img-thumbnail w-100 h-25' alt='Thumbnail Photo'/>
              </p>
            </div>
          </div>
        </React.Fragment>
    );
  }
}
