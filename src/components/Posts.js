import React, {Component} from 'react';
import Post from './Post';
import Prev from './Prev';
import Next from './Next';
import ApiRequests from './services/fetch-data';
import './comment.css'

const PER_PAGE = 10;
export default class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      users: [],
      currentPage: 1,
      postsPerPage: 10,
      links: 1,
      paginatedPosts: [],
      showComments: false,
      paginatedData : []
    };
  }
  async componentWillMount() {

    await Promise.all(ApiRequests).then(([posts, comments, users]) => {
      this.setState({ posts, comments, users });
    })
    .catch(err => {
      console.log(err);
    });

    this.createPaginatedData();

    const startingPage = this.props.startingPage || 1;
    const posts = this.state.posts;
    let pageCount = posts.length / PER_PAGE;
    if (posts.length % PER_PAGE > 0) {
      pageCount++;
    }
    this.setState({
      currentPage: startingPage,
      pageCount: pageCount
    });
  }
  nextPage = () => {
    if(this.state.currentPage < 10){
      this.setState({ currentPage: ++this.state.currentPage });
      this.createPaginatedData();
      console.log(this.state.currentPage, this.state.pageCount)
    }
  };
  previousPage = () => {
    if(this.state.currentPage > 1){
      this.setState({ currentPage: --this.state.currentPage });
      this.createPaginatedData()
    }
  };
  componentDidMount() {
    document.querySelectorAll("a[href^='#']").forEach(function(element) {
      element.addEventListener('click', e => {
        e.preventDefault();
      });
    })
  }

  createPaginatedData() {
    const upperLimit = this.state.currentPage * PER_PAGE;
    
    this.setState({paginatedData : this.state.posts.slice(upperLimit - PER_PAGE, upperLimit)}) ;
  }

  getUser(userId) {
    return this.state.users.find(user => user.id === userId);
  }
  getComments(postId) {
    return this.state.comments.filter(comment => comment.postId === postId);
  }

  getPaginationLinks() {
    let numberOfPagesAvailable = this.state.posts.length / PER_PAGE;

    return [
      <Prev posts={this} currentPage={this.state.currentPage} setCurrentPage={this.setCurrentPage}/>,
      <Next posts={this} numberOfPagesAvailable={numberOfPagesAvailable} currentPage={this.state.currentPage}/>
    ];
  }
  toggleComments() {
    this.setState({ showComments: !this.state.showComments });
  }

  render() {

    return (
        <React.Fragment>
          <div className='row sticky-top bg-white py-2 mb-2 border-bottom border-danger'>
            <div className='col-md-8'>
              <ul className='w-100 pagination'>{this.getPaginationLinks()}</ul>
            </div>
            <div className='col-md-4 comment-button' >
              <div className='input-group w-100'>
                <div className='input-group-prepend'>
                  <div className='input-group-text'>C</div>
                </div>
                <div className='input-group-append w-25'>
                  <div className='input-group-text'>
                    <input
                        type='checkbox'
                        onClick={() => this.toggleComments()}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='posts mt-3'>
            {this.state.paginatedData.map(post => (
                <Post
                    showComments={this.state.showComments}
                    post={post}
                    comments={this.getComments(post.id)}
                    user={this.getUser(post.userId)}
                />
            ))}
          </div>
        </React.Fragment>
    );
  }
}
