import React from 'react';
const Notfound = () => (
    <div className='row justify-content-center'>
      <div className='col-md-4 mx-auto'>
        <h1>404</h1>
        <p>
          <small>Page Not Found</small>
        </p>
      </div>
    </div>
);
export default Notfound;
