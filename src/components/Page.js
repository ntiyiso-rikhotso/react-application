import React from 'react'
import Breadcrumb from './Breadcrumb';
import PageRequest from './PageRequest';

export default class Page extends React.Component{
   render(){
     return (
         <React.Fragment>
           <Breadcrumb/>
           <PageRequest screen={this.props.screen} />
         </React.Fragment>
     );
   }
}

