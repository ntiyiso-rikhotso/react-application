import React from 'react'
 const Prev = props => {
   return (
       <li className={`text-center w-100 page-item`}>
         <a onClick={props.posts.previousPage} disabled={props.currentPage === 1 ? 'disabled' : 'false'} href='#' className={`page-link`} >
           &laquo; Prev 10 posts
         </a>
       </li>
   );
};

export default Prev;
