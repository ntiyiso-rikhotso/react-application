import React, { Component } from 'react';
export default class Nav extends Component {
  render() {
    return (
        <React.Fragment>
          <nav className='navbar navbar-expand-lg navbar-dark bg-primary'>
            <div className='container'>
              <a href='/' className='navbar-brand'>
                FRO
              </a>
              <button
                  className='navbar-toggler'
                  type='button'
                  data-toggle='collapse'
                  data-target='#navbarResponsive'
                  aria-controls='navbarResponsive'
                  aria-expanded='false'
                  aria-label='Toggle navigation'>
                <span className='navbar-toggler-icon' />
              </button>
            </div>
          </nav>
        </React.Fragment>
    );
  }
}
