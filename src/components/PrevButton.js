import React, {Component} from 'react';

export default class PrevButton extends Component {
  render() {
    return (
        <React.Fragment>
          <li
              className={`text-center w-100 page-item`}>
            <a disabled={this.parent.state.currentPage === 1 ?
                'disabled' :
                'false'} href='#' className={`page-link`} onClick={() => {
              this.setCurrentPage(this.state.currentPage - 1);
            }}>
              &laquo; Prev 10 posts
            </a>
          </li>
        </React.Fragment>
    );

  }
}
