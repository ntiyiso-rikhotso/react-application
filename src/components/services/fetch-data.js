const ApiRequests = ['posts', 'comments', 'users'].map((endpoint) => {
  return fetch(`https://jsonplaceholder.typicode.com/${endpoint}`).then(data => data.json());
});

export default ApiRequests;