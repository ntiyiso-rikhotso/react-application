import React from 'react';

const Breadcrumb = _ => {
  return (
      <ol className='breadcrumb'>
        <li className='breadcrumb-item'>
          <a href='/'>Home</a>
        </li>
        <li className='breadcrumb-item active'>Posts</li>
      </ol>);
};

export default Breadcrumb;