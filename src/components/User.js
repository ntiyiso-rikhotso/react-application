import React, { Component } from 'react';
import Album from './Album';
import Breadcrumb from './Breadcrumb';
import ApiRequests from './services/fetch-data';

export default class User extends Component {
  constructor(props) {
    super(props);

    console.log(props)
    this.state = {
      albums: [],
      photos: [],
      user: {}
    };
  }

  async componentWillMount() {
    
    await Promise.all(ApiRequests).then(([posts, comments, users]) => {
      this.setState({ posts, comments, users });
    })
    .catch(err => {
      console.log(err);
    });
    this.getCurrentUser();
  }

  getCurrentUser(){
    let id = parseInt(this.props.match.params.id);
    let user = this.state.users.find(user => user.id === id);
     
    this.setState({user})
  }

  findAlbumThumbnailPhoto(albumId) {
    return this.state.photos.find(photo => photo.albumId === albumId);
  }

  render() {
    const user = this.state.user;

    return (
        <React.Fragment>
          <Breadcrumb/>
          <div className='row justify-content-center'>
            <div className='col-md-10'>
              <table className='table table-borderless'>
                <tbody>
                {user ? (
                    <React.Fragment>
                      <tr className='border-bottom'>
                        <td>Name</td>
                        <td>: {user.name}</td>
                      </tr>
                      <tr className='border-bottom'>
                        <td>Website</td>
                        <td>: {user.website}</td>
                      </tr>
                      <tr className='border-right'>
                        <td>Albums</td>
                        <td>
                          <p>
                            <strong>
                              Below is the list of albums for {user.name}
                            </strong>
                          </p>
                          {this.state.albums.map(album => {
                            return <Album imageSrc={this.findAlbumThumbnailPhoto(album.id).thumbnailUrl} title={album.title}/>;
                          })}
                        </td>
                      </tr>
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                      <tr>
                        <td className='text-danger text-center'>
                          Sorry, The Selected User Is Not Found
                        </td>
                      </tr>
                    </React.Fragment>
                )}
                </tbody>
              </table>
            </div>
          </div>
        </React.Fragment>
    );
  }
}
