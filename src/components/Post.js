import React, { Component } from 'react';
import Comment from './Comment';

const styles = {
   width : '40px'
};

export default class Post extends Component {
  render() {
    return (
        <React.Fragment>
          <div className="media mb-3">
            <a href={`user/${this.props.user.id}`}>
              <img style={styles} className="mr-3 profile-image" src="https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/10_avatar-512.png" alt="Generic placeholder image"/>
            </a>
            <div className="media-body">
              <h5 className="mt-0">{this.props.post.title}</h5>
              {this.props.post.body}

              <p className='text-right text-danger'>
                <small>{this.props.comments.length} comments</small>
              </p>
              {this.props.showComments
                  ? this.props.comments.map(_item => (
                      <Comment userName={_item.name} body={_item.body} />
                  ))
                  : ''}

            </div>
          </div>
        </React.Fragment>
    );
  }
}
