import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Posts from "./components/Posts";
import User from "./components/User";
import Nav from "./components/Nav";
import NotFound404 from "./components/NotFound404";

import 'bootswatch/dist/lux/bootstrap.min.css';



const routing = (
    <Router>
      <React.Fragment>
        <Nav />
        <div className="container pt-2">
          <Switch>
            <Route exact path="/" render={props => <Posts {...props} />} />
            <Route path="/user/:id" render={props => <User {...props} />} />
            <Route path="*" component={NotFound404} />
          </Switch>
        </div>
      </React.Fragment>
    </Router>
);
ReactDOM.render(routing, document.getElementById("root"));
